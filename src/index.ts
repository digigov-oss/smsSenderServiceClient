import soapClient from './soapClient.js';
import {
    generateAuditRecord,
    AuditRecord,
    FileEngine,
    AuditEngine,
} from '@digigov-oss/gsis-audit-record-db';

import config from './config.json';

export type AuditInit = AuditRecord;

export type GetSmsAccountInfoOutputRecord = {
    afm?: string;
    description?: string;
    defaultCredits?: number;
    remainingCredits?: number;
    maxMessageLength?: number;
    restrictNumbers?: string;
    lastCallDateStr?: string;
    environment?: string;
    expirationDateStr?: string;
    isActive?: boolean;
};

export type SendSmsToMobileOutputRecord = {
    status?: string;
    message?: string;
    credits?: number;
};

export type SendSmsToMobileAltOutputRecord = {
    status?: string;
    message?: string;
    id?: string;
};

export type ErrorRecord = {
    errorCode: string;
    errorDescr: string;
};

/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export type Overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};

/**
 * Get SMS account info
 * @param accountAfm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns AuditRecord | errorRecord
 */
export const getSmsAccountInfo = async (
    accountAfm: string,
    user: string,
    pass: string,
    overrides?: Overrides,
) => {
    const endpoint = overrides?.endpoint ?? '';
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? ({} as AuditRecord);
    const auditStoragePath = overrides?.auditStoragePath ?? '/tmp';
    const auditEngine =
        overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord) throw new Error('Audit record is not initialized');

    const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
    const response = await s.getSmsAccountInfo(accountAfm);
    return {
        kedResponse: response,
        auditRecord: auditRecord,
    };
};

/**
 * Get SMS account info
 * @param accountAfm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns AuditRecord | errorRecord
 */
export const sendSmsToMobile = async (
    accountAfm: string,
    mobile: string,
    message: string,
    user: string,
    pass: string,
    overrides?: Overrides,
) => {
    const endpoint = overrides?.endpoint ?? '';
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? ({} as AuditRecord);
    const auditStoragePath = overrides?.auditStoragePath ?? '/tmp';
    const auditEngine =
        overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord) throw new Error('Audit record is not initialized');

    const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
    const response = await s.sendSmsToMobile(accountAfm, mobile, message);
    return {
        kedResponse: response,
        auditRecord: auditRecord,
    };
};

/**
 * Get SMS account info
 * @param accountAfm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns AuditRecord | errorRecord
 */
export const sendSmsToMobileAlt = async (
    mobile: string,
    message: string,
    user: string,
    pass: string,
    overrides?: Overrides,
) => {
    const endpoint = overrides?.endpoint ?? '';
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? ({} as AuditRecord);
    const auditStoragePath = overrides?.auditStoragePath ?? '/tmp';
    const auditEngine =
        overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord) throw new Error('Audit record is not initialized');

    const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
    const response = await s.sendSmsToMobileAlt(mobile, message);
    return {
        kedResponse: response,
        auditRecord: auditRecord,
    };
};

export default sendSmsToMobile;
