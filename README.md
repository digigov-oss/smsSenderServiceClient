# Client for Sms sender service of KED

Client to connect to the SMS sender service, useful for nextjs/nodejs projects.

#### Example:

```
import sendSMSToMobile from '@digigov-oss/sms-sender-service-client';

const test = async () => {
   const overrides = {
        prod:false,
        auditInit: {
            auditUnit: 'grnet.gr',
        },
        auditStoragePath: '/auditStorage',
    }

    try {
        const result = await sendSMSToMobile("accountAfm", "mobile", "message", "username", "password",overrides);
        return result;
    } catch (error) {
        console.log(error);
    }
}

test().then((x) => { console.log('response', x); });
```

-   you can use `overrides` to override the default values
-   for your tests, you don't need to use the `overrides` mecahnism,in that case, the default storage path will be used ie `/tmp`
-   look at [KED](https://www.gsis.gr/dimosia-dioikisi/ked/) standard guides for records you can use on auditInit"
    Also, you can use `overrides` to override the default storage engine.

```
import sendSMSToMobile from '@digigov-oss/get-nnc-identity-Ext-client';
import {PostgreSqlEngine} from '@digigov-oss/gsis-audit-record-db';

const test = async () => {
    try {
        const overrides = {
        auditEngine: new PostgreSqlEngine('postgres://postgres:postgres@localhost:5432/postgres'),
        auditInit: {
            auditUnit: 'grnet.gr',
        },
        }
        const result = await sendSMSToMobile("accountAfm", "mobile", "message", "username", "password",overrides);
        return result;
    } catch (error) {
        console.log(error);
    }
}

test().then((x) => { console.log('response', x); });

```

Look at module [AuditRecordDB](https://gitlab.grnet.gr/digigov-oss/auditRecordDB/-/blob/main/README.md) for more details on how to use the AuditEngine.

If you plan to use only the `FileEngine`, you can skip the installation of other engines by ignoring optional dependencies.
i.e.` yarn install --ignore-optional`

#### Returns

an object like the following:

```
{
    status: '1',
    message: 'Δοκιμαστικό μήνυμα", 
    credits: '85'
}
```

or an error message like:

```
{ message: 'Ο Α.Φ.Μ. δεν είναι καταχωρημένος' }
```

#### Available Mobile Numbers for testing:

0000000000

##### \* Notes

you have to ask KED for smsSenderService_KED_v_0.91 documentation to get more info about the output and error fields.

#### \* known issues

If the endpoint is not correct, you can override it by setting the `endpoint` property on the `overrides` object.

```
const overrides = {
    endpoint: 'https://ked.gsis.gr/esb/...',
}
```
