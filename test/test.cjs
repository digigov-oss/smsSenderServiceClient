const sendSmsToMobile = require('../dist/cjs/index.js').default;
const config = require('./config.json');

const test = async () => {
    try {
        const result = await sendSmsToMobile(
            '979720100',
            '0000000000',
            'Δοκιμαστικό μήνυμα',
            config.user,
            config.pass,
        );
        return result;
    } catch (error) {
        console.log(error);
    }
};

test().then((x) => {
    console.log('response', x);
});
