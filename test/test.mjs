import sendSmsToMobile from '../dist/esm/index.js';
import config from './config.json' assert { type: 'json' };

const test = async () => {
    try {
        const result = await sendSmsToMobile(
            '979720100',
            '0000000000',
            'Δοκιμαστικό μήνυμα',
            config.user,
            config.pass,
        );
        return result;
    } catch (error) {
        console.log(error);
    }
};

test().then((x) => {
    console.log('response', x);
});
