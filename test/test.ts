import {
    sendSmsToMobile,
    sendSmsToMobileAlt,
    getSmsAccountInfo,
} from '../src/index';
import config from './config.json';

const test = async () => {
    try {
        const result = await getSmsAccountInfo(
            '979720100',
            config.user,
            config.pass,
        );
        return result;
    } catch (error) {
        console.log(error);
    }

    try {
        const result = await sendSmsToMobileAlt(
            '0000000000',
            'Δοκιμαστικό μήνυμα',
            config.user,
            config.pass,
        );
        return result;
    } catch (error) {
        console.log(error);
    }

    try {
        const result = await sendSmsToMobile(
            '979720100',
            '0000000000',
            'Δοκιμαστικό μήνυμα',
            config.user,
            config.pass,
        );
        return result;
    } catch (error) {
        console.log(error);
    }
};

test().then((x) => {
    console.log('response', x);
});
