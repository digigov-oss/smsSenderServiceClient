import sendSmsToMobile from '../src/index';
import config from './config.json';

import { PostgreSqlEngine } from '@digigov-oss/gsis-audit-record-db';

const test = async () => {
    try {
        const overrides = {
            auditEngine: new PostgreSqlEngine(
                'postgres://postgres:postgres@localhost:5432/postgres',
            ),
            auditInit: {
                auditUnit: 'grnet.gr',
            },
        };
        const result = await sendSmsToMobile(
            '979720100',
            '0000000000',
            'Δοκιμαστικό μήνυμα',
            config.user,
            config.pass,
            overrides,
        );
        return result;
    } catch (error) {
        console.log(error);
    }
};

test().then((x) => {
    console.log('response', x);
});
