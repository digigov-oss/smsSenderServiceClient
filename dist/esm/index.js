import soapClient from './soapClient.js';
import { generateAuditRecord, FileEngine, } from '@digigov-oss/gsis-audit-record-db';
import config from './config.json';
/**
 * Get SMS account info
 * @param accountAfm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns AuditRecord | errorRecord
 */
export const getSmsAccountInfo = async (accountAfm, user, pass, overrides) => {
    const endpoint = overrides?.endpoint ?? '';
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? {};
    const auditStoragePath = overrides?.auditStoragePath ?? '/tmp';
    const auditEngine = overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord)
        throw new Error('Audit record is not initialized');
    const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
    const response = await s.getSmsAccountInfo(accountAfm);
    return {
        kedResponse: response,
        auditRecord: auditRecord,
    };
};
/**
 * Get SMS account info
 * @param accountAfm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns AuditRecord | errorRecord
 */
export const sendSmsToMobile = async (accountAfm, mobile, message, user, pass, overrides) => {
    const endpoint = overrides?.endpoint ?? '';
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? {};
    const auditStoragePath = overrides?.auditStoragePath ?? '/tmp';
    const auditEngine = overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord)
        throw new Error('Audit record is not initialized');
    const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
    const response = await s.sendSmsToMobile(accountAfm, mobile, message);
    return {
        kedResponse: response,
        auditRecord: auditRecord,
    };
};
/**
 * Get SMS account info
 * @param accountAfm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns AuditRecord | errorRecord
 */
export const sendSmsToMobileAlt = async (mobile, message, user, pass, overrides) => {
    const endpoint = overrides?.endpoint ?? '';
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? {};
    const auditStoragePath = overrides?.auditStoragePath ?? '/tmp';
    const auditEngine = overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord)
        throw new Error('Audit record is not initialized');
    const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
    const response = await s.sendSmsToMobileAlt(mobile, message);
    return {
        kedResponse: response,
        auditRecord: auditRecord,
    };
};
export default sendSmsToMobile;
