import thesoap from 'soap';
let soap = thesoap;
try {
    soap = require('soap');
}
catch (error) {
    //my hackish way to make soap work on both esm and cjs
    //theshoap on esm is undefined
    //On esm require is not defined, however on cjs require can be used.
    //So we try to use require and if it fails we use the thesoap module
}
/**
 * SOAP client
 * @class Soap
 * @description SOAP client
 * @param {string} wsdl - The URL of the SOAP service
 * @param {string} username
 * @param {string} password
 * @param {AuditRecord} auditRecord
 * @param {string} endpoint
 */
class Soap {
    _wsdl;
    _username;
    _password;
    _auditRecord;
    _endpoint;
    constructor(wsdl, username, password, auditRecord, endpoint) {
        this._wsdl = wsdl;
        this._username = username;
        this._password = password;
        this._auditRecord = auditRecord;
        this._endpoint = endpoint;
    }
    async init() {
        try {
            const client = await soap.createClientAsync(this._wsdl, {
                wsdl_headers: {
                    Authorization: 'Basic ' +
                        Buffer.from(`${this._username}:${this._password}`).toString('base64'),
                },
            });
            if (this._endpoint) {
                client.setEndpoint(this._endpoint);
            }
            return client;
        }
        catch (e) {
            throw e;
        }
    }
    async getSmsAccountInfo(accountAfm) {
        try {
            const client = await this.init();
            var options = {
                hasNonce: true,
                actor: 'actor',
            };
            var wsSecurity = new soap.WSSecurity(this._username, this._password, options);
            client.setSecurity(wsSecurity);
            const auditRecord = this._auditRecord;
            const args = {
                auditRecord: auditRecord,
                getSmsAccountInfoInputRecord: {
                    accountAfm: accountAfm,
                },
            };
            const result = await client.getSmsAccountInfoAsync(args);
            const errorRecord = result[0].errorRecord;
            if (errorRecord) {
                return errorRecord;
            }
            else {
                return result[0]
                    .getSmsAccountInfoOutputRecord;
            }
        }
        catch (e) {
            throw e;
        }
    }
    async sendSmsToMobile(accountAfm, mobile, message) {
        try {
            const client = await this.init();
            var options = {
                hasNonce: true,
                actor: 'actor',
            };
            var wsSecurity = new soap.WSSecurity(this._username, this._password, options);
            client.setSecurity(wsSecurity);
            const auditRecord = this._auditRecord;
            const args = {
                auditRecord: auditRecord,
                sendSmsToMobileInputRecord: {
                    accountAfm: accountAfm,
                    mobile: mobile,
                    message: message,
                },
            };
            const result = await client.sendSmsToMobileAsync(args);
            const errorRecord = result[0].errorRecord;
            if (errorRecord) {
                return errorRecord;
            }
            else {
                return result[0]
                    .sendSmsToMobileOutputRecord;
            }
        }
        catch (e) {
            throw e;
        }
    }
    async sendSmsToMobileAlt(mobile, message) {
        try {
            const client = await this.init();
            var options = {
                hasNonce: true,
                actor: 'actor',
            };
            var wsSecurity = new soap.WSSecurity(this._username, this._password, options);
            client.setSecurity(wsSecurity);
            const auditRecord = this._auditRecord;
            const args = {
                auditRecord: auditRecord,
                sendSmsToMobileAltInputRecord: {
                    mobile: mobile,
                    message: message,
                },
            };
            const result = await client.sendSmsToMobileAltAsync(args);
            const errorRecord = result[0].errorRecord;
            if (errorRecord) {
                return errorRecord;
            }
            else {
                return result[0]
                    .sendSmsToMobileAltOutputRecord;
            }
        }
        catch (e) {
            throw e;
        }
    }
}
export default Soap;
