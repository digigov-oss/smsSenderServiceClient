import { AuditRecord } from '@digigov-oss/gsis-audit-record-db';
import { ErrorRecord, GetSmsAccountInfoOutputRecord, SendSmsToMobileAltOutputRecord, SendSmsToMobileOutputRecord } from './index';
/**
 * SOAP client
 * @class Soap
 * @description SOAP client
 * @param {string} wsdl - The URL of the SOAP service
 * @param {string} username
 * @param {string} password
 * @param {AuditRecord} auditRecord
 * @param {string} endpoint
 */
declare class Soap {
    private _wsdl;
    private _username;
    private _password;
    private _auditRecord;
    private _endpoint;
    constructor(wsdl: string, username: string, password: string, auditRecord: AuditRecord, endpoint: string);
    init(): Promise<any>;
    getSmsAccountInfo(accountAfm: string): Promise<ErrorRecord | GetSmsAccountInfoOutputRecord>;
    sendSmsToMobile(accountAfm: string, mobile: string, message: string): Promise<ErrorRecord | SendSmsToMobileOutputRecord>;
    sendSmsToMobileAlt(mobile: string, message: string): Promise<ErrorRecord | SendSmsToMobileAltOutputRecord>;
}
export default Soap;
