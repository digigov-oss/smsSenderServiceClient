import { AuditRecord, AuditEngine } from '@digigov-oss/gsis-audit-record-db';
export type AuditInit = AuditRecord;
export type GetSmsAccountInfoOutputRecord = {
    afm?: string;
    description?: string;
    defaultCredits?: number;
    remainingCredits?: number;
    maxMessageLength?: number;
    restrictNumbers?: string;
    lastCallDateStr?: string;
    environment?: string;
    expirationDateStr?: string;
    isActive?: boolean;
};
export type SendSmsToMobileOutputRecord = {
    status?: string;
    message?: string;
    credits?: number;
};
export type SendSmsToMobileAltOutputRecord = {
    status?: string;
    message?: string;
    id?: string;
};
export type ErrorRecord = {
    errorCode: string;
    errorDescr: string;
};
/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export type Overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};
/**
 * Get SMS account info
 * @param accountAfm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns AuditRecord | errorRecord
 */
export declare const getSmsAccountInfo: (accountAfm: string, user: string, pass: string, overrides?: Overrides) => Promise<{
    kedResponse: ErrorRecord | GetSmsAccountInfoOutputRecord;
    auditRecord: AuditRecord;
}>;
/**
 * Get SMS account info
 * @param accountAfm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns AuditRecord | errorRecord
 */
export declare const sendSmsToMobile: (accountAfm: string, mobile: string, message: string, user: string, pass: string, overrides?: Overrides) => Promise<{
    kedResponse: ErrorRecord | SendSmsToMobileOutputRecord;
    auditRecord: AuditRecord;
}>;
/**
 * Get SMS account info
 * @param accountAfm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns AuditRecord | errorRecord
 */
export declare const sendSmsToMobileAlt: (mobile: string, message: string, user: string, pass: string, overrides?: Overrides) => Promise<{
    kedResponse: ErrorRecord | SendSmsToMobileAltOutputRecord;
    auditRecord: AuditRecord;
}>;
export default sendSmsToMobile;
