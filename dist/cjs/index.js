"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendSmsToMobileAlt = exports.sendSmsToMobile = exports.getSmsAccountInfo = void 0;
const soapClient_js_1 = __importDefault(require("./soapClient.js"));
const gsis_audit_record_db_1 = require("@digigov-oss/gsis-audit-record-db");
const config_json_1 = __importDefault(require("./config.json"));
/**
 * Get SMS account info
 * @param accountAfm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns AuditRecord | errorRecord
 */
const getSmsAccountInfo = (accountAfm, user, pass, overrides) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b, _c, _d, _e;
    const endpoint = (_a = overrides === null || overrides === void 0 ? void 0 : overrides.endpoint) !== null && _a !== void 0 ? _a : '';
    const prod = (_b = overrides === null || overrides === void 0 ? void 0 : overrides.prod) !== null && _b !== void 0 ? _b : false;
    const auditInit = (_c = overrides === null || overrides === void 0 ? void 0 : overrides.auditInit) !== null && _c !== void 0 ? _c : {};
    const auditStoragePath = (_d = overrides === null || overrides === void 0 ? void 0 : overrides.auditStoragePath) !== null && _d !== void 0 ? _d : '/tmp';
    const auditEngine = (_e = overrides === null || overrides === void 0 ? void 0 : overrides.auditEngine) !== null && _e !== void 0 ? _e : new gsis_audit_record_db_1.FileEngine(auditStoragePath);
    const wsdl = prod == true ? config_json_1.default.prod.wsdl : config_json_1.default.test.wsdl;
    const auditRecord = yield (0, gsis_audit_record_db_1.generateAuditRecord)(auditInit, auditEngine);
    if (!auditRecord)
        throw new Error('Audit record is not initialized');
    const s = new soapClient_js_1.default(wsdl, user, pass, auditRecord, endpoint);
    const response = yield s.getSmsAccountInfo(accountAfm);
    return {
        kedResponse: response,
        auditRecord: auditRecord,
    };
});
exports.getSmsAccountInfo = getSmsAccountInfo;
/**
 * Get SMS account info
 * @param accountAfm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns AuditRecord | errorRecord
 */
const sendSmsToMobile = (accountAfm, mobile, message, user, pass, overrides) => __awaiter(void 0, void 0, void 0, function* () {
    var _f, _g, _h, _j, _k;
    const endpoint = (_f = overrides === null || overrides === void 0 ? void 0 : overrides.endpoint) !== null && _f !== void 0 ? _f : '';
    const prod = (_g = overrides === null || overrides === void 0 ? void 0 : overrides.prod) !== null && _g !== void 0 ? _g : false;
    const auditInit = (_h = overrides === null || overrides === void 0 ? void 0 : overrides.auditInit) !== null && _h !== void 0 ? _h : {};
    const auditStoragePath = (_j = overrides === null || overrides === void 0 ? void 0 : overrides.auditStoragePath) !== null && _j !== void 0 ? _j : '/tmp';
    const auditEngine = (_k = overrides === null || overrides === void 0 ? void 0 : overrides.auditEngine) !== null && _k !== void 0 ? _k : new gsis_audit_record_db_1.FileEngine(auditStoragePath);
    const wsdl = prod == true ? config_json_1.default.prod.wsdl : config_json_1.default.test.wsdl;
    const auditRecord = yield (0, gsis_audit_record_db_1.generateAuditRecord)(auditInit, auditEngine);
    if (!auditRecord)
        throw new Error('Audit record is not initialized');
    const s = new soapClient_js_1.default(wsdl, user, pass, auditRecord, endpoint);
    const response = yield s.sendSmsToMobile(accountAfm, mobile, message);
    return {
        kedResponse: response,
        auditRecord: auditRecord,
    };
});
exports.sendSmsToMobile = sendSmsToMobile;
/**
 * Get SMS account info
 * @param accountAfm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns AuditRecord | errorRecord
 */
const sendSmsToMobileAlt = (mobile, message, user, pass, overrides) => __awaiter(void 0, void 0, void 0, function* () {
    var _l, _m, _o, _p, _q;
    const endpoint = (_l = overrides === null || overrides === void 0 ? void 0 : overrides.endpoint) !== null && _l !== void 0 ? _l : '';
    const prod = (_m = overrides === null || overrides === void 0 ? void 0 : overrides.prod) !== null && _m !== void 0 ? _m : false;
    const auditInit = (_o = overrides === null || overrides === void 0 ? void 0 : overrides.auditInit) !== null && _o !== void 0 ? _o : {};
    const auditStoragePath = (_p = overrides === null || overrides === void 0 ? void 0 : overrides.auditStoragePath) !== null && _p !== void 0 ? _p : '/tmp';
    const auditEngine = (_q = overrides === null || overrides === void 0 ? void 0 : overrides.auditEngine) !== null && _q !== void 0 ? _q : new gsis_audit_record_db_1.FileEngine(auditStoragePath);
    const wsdl = prod == true ? config_json_1.default.prod.wsdl : config_json_1.default.test.wsdl;
    const auditRecord = yield (0, gsis_audit_record_db_1.generateAuditRecord)(auditInit, auditEngine);
    if (!auditRecord)
        throw new Error('Audit record is not initialized');
    const s = new soapClient_js_1.default(wsdl, user, pass, auditRecord, endpoint);
    const response = yield s.sendSmsToMobileAlt(mobile, message);
    return {
        kedResponse: response,
        auditRecord: auditRecord,
    };
});
exports.sendSmsToMobileAlt = sendSmsToMobileAlt;
exports.default = exports.sendSmsToMobile;
